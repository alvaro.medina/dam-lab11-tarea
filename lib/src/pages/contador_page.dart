import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  @override
  createState() {
    return _ContadorPageState();
  }
}

class _ContadorPageState extends State<ContadorPage> {
  int _conteo = 0;

  void _increment() {
    setState(() {
      _conteo++;
    });
  }

  void _decrease() {
    setState(() {
      _conteo--;
    });
  }

  void _reset() {
    setState(() {
      _conteo = 0;
    });
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Stateful',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Stateful'),
          ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Numero de clicks', style: TextStyle(fontSize: 25)),
            Text('$_conteo',style: TextStyle(fontSize: 25))
          ],
         ),
         ),
         floatingActionButton: Stack(
          children: <Widget>[
          Align(
              alignment: Alignment(-0.5, 1.0),
              child: FloatingActionButton(
                onPressed: _decrease,
                child: Icon(Icons.remove),
              ),
            ),
          Align(
              alignment: Alignment(0.0, 1.0),
              child: FloatingActionButton(
                onPressed: _reset,
                child: Icon(Icons.refresh),
              ),
            ),
          Align(
              alignment: Alignment(0.5, 1.0),
              child: FloatingActionButton(
                onPressed: _increment,
                child: Icon(Icons.add),
              ),
            ),
          ],
        )
        
       ),
      );
  }
}
